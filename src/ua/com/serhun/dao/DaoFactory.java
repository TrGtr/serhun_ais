package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoFactory {
	private static DaoFactory instance;

	private final String mySqlProtocol = "jdbc:mysql://localhost:3306/";
	private final String dbName = "mydb";
	private final String jdbcURL = mySqlProtocol + dbName;
	private final String mySqlDriver = "com.mysql.jdbc.Driver";
	final String driver = mySqlDriver;

	private DaoFactory() {
	}

	public Connection getConnection() throws SQLException {
		System.setProperty("jdbc.drivers", driver);
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		connection = DriverManager.getConnection(jdbcURL, "root", "1234");
		return connection;
	}

	public static DaoFactory getInstance() {
		if (instance == null) {
			instance = new DaoFactory();
		}
		return instance;
	}
}
