package ua.com.serhun.dao;

import java.util.List;

import ua.com.serhun.domain.Product;

public interface ProductDao {

	List<Product> getAllProducts() throws DAOException;

	void addProduct(String name, String description, int price)
			throws DAOException;

	Product getProduct(int id) throws DAOException;

	void updateProduct(int id, String name, String description, int price)
			throws DAOException;

	void deleteProduct(int id) throws DAOException;

}
