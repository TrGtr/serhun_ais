package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ua.com.serhun.domain.Customer;

public class CustomerDaoImpl implements CustomerDao {

	private DaoFactory daoFactory = DaoFactory.getInstance();
	private static Logger log = Logger.getLogger(CustomerDaoImpl.class
			.getName());

	@Override
	public List<Customer> getAllCustomers() throws DAOException {
		log.info("Getting all customers");
		String sql = "SELECT * FROM customer;";

		List<Customer> customers = new ArrayList<Customer>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			log.info("Adding customers to list to return");
			while (resultSet.next()) {
				customers.add(new Customer(Integer.parseInt(resultSet
						.getString("idcustomer")),
						resultSet.getString("name"), resultSet
								.getString("email"), resultSet
								.getString("phone"), resultSet
								.getString("address")));
			}
		} catch (SQLException e) {
			log.warning("Cannot get customers:" + e.getMessage());
			throw new DAOException("Cannot get customers", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					log.info("Result set closed");
				}
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning customers list");
		return customers;
	}

	@Override
	public void addCustomer(String name, String email, String phone,
			String address, String pass) throws DAOException {
		log.info("Adding new customer");
		String sql = "INSERT INTO customer (name, email, phone, address, password) VALUES (?,?,?,?,?);";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, email);
			statement.setString(3, phone);
			statement.setString(4, address);
			statement.setString(5, pass);
			statement.execute();
			log.info("customer added");
		} catch (SQLException e) {
			log.warning("Cannot add customer:" + e.getMessage());
			throw new DAOException("Cannot add customer", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public Customer getCustomer(int id) throws DAOException {
		log.info("Looking for customer");
		String sql = "SELECT * FROM customer WHERE idcustomer = ?;";

		Customer customer = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create customer to return");

				customer = new Customer(Integer.parseInt(resultSet
						.getString("idcustomer")),
						resultSet.getString("name"),
						resultSet.getString("email"),
						resultSet.getString("phone"),
						resultSet.getString("address"),
						resultSet.getString("password"));

			} else return null;
		} catch (SQLException e) {
			log.warning("Cannot get customer:" + e.getMessage());
			throw new DAOException("Cannot get customer", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning customer");
		return customer;
	}
	
	
	@Override
	public Customer getCustomer(String phone) throws DAOException {
		log.info("Looking for customer");
		String sql = "SELECT * FROM customer WHERE phone = ?;";

		Customer customer = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, phone);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create customer to return");

				customer = new Customer(Integer.parseInt(resultSet
						.getString("idcustomer")),
						resultSet.getString("name"),
						resultSet.getString("email"),
						resultSet.getString("phone"),
						resultSet.getString("address"),
						resultSet.getString("password"));

			}
		} catch (SQLException e) {
			log.warning("Cannot get customer:" + e.getMessage());
			throw new DAOException("Cannot get customer", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning customer");
		return customer;
	}

	@Override
	public void updateCustomer(int id, String name, String email, String phone,
			String address) throws DAOException {
		log.info("Trying to update customer");
		String sql = "UPDATE customer SET name = ?, email = ?, phone = ?, address = ? WHERE idcustomer = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, email);
			statement.setString(3, phone);
			statement.setString(4, address);
			statement.setString(5, Integer.toString(id));
			statement.execute();
			log.info("customer updated");
		} catch (SQLException e) {
			log.warning("Cannot update customer:" + e.getMessage());
			throw new DAOException("Cannot update customer", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void updateCustomer(int id, String name, String email, String phone,
			String address, String pass) throws DAOException {
		log.info("Trying to update customer");
		String sql = "UPDATE customer SET name = ?, email = ?, phone = ?, address = ?, password = ? WHERE idcustomer = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, email);
			statement.setString(3, phone);
			statement.setString(4, address);
			statement.setString(5, pass);
			statement.setString(6, Integer.toString(id));
			statement.execute();
			log.info("customer updated");
		} catch (SQLException e) {
			log.warning("Cannot update customer:" + e.getMessage());
			throw new DAOException("Cannot update customer", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void deleteCustomer(int id) throws DAOException {
		log.info("Trying to delete customer");
		String sql = "DELETE FROM customer WHERE idcustomer = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			statement.execute();
			log.info("customer deleted");
		} catch (SQLException e) {
			log.warning("Cannot delete customer:" + e.getMessage());
			throw new DAOException("Cannot delete customer", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

}
