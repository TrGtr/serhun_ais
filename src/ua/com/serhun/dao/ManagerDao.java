package ua.com.serhun.dao;

import java.util.List;

import ua.com.serhun.domain.Manager;

public interface ManagerDao {

	List<Manager> getAllManagers() throws DAOException;

	void addManager(String name, String phone, String pass)
			throws DAOException;

	Manager getManager(int id) throws DAOException;
	
	Manager getManager(String phone) throws DAOException;

	void updateManager(int id, String name, String phone, String pass)
			throws DAOException;
	
	void updateManager(int id, String name, String phone)
			throws DAOException;

	void deleteManager(int id) throws DAOException;

}
