package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ua.com.serhun.domain.Item;

public class ItemDaoImpl implements ItemDao {

	private DaoFactory daoFactory = DaoFactory.getInstance();
	private static Logger log = Logger.getLogger(ItemDaoImpl.class.getName());

	@Override
	public List<Item> getAllItems() throws DAOException {
		log.info("Getting all items");
		String sql = "SELECT I.iditem, P.idproduct, P.name, P.description, I.color, P.price, I.idorder FROM item I "
				+ "INNER JOIN product P " + "ON P.idproduct = I.idproduct;";

		List<Item> items = new ArrayList<Item>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			log.info("Adding items to list to return");
			while (resultSet.next()) {
				int orderid = 0;
				try {
					orderid = Integer.parseInt(resultSet.getString("idorder"));
				} catch (Exception e) {
					orderid = 0;
				}
				items.add(new Item(Integer.parseInt(resultSet
						.getString("iditem")), Integer.parseInt(resultSet
						.getString("idproduct")), resultSet.getString("name"),
						resultSet.getString("description"), resultSet
								.getString("color"), resultSet
								.getString("price"), orderid));
			}
		} catch (SQLException e) {
			log.warning("Cannot get items:" + e.getMessage());
			throw new DAOException("Cannot get items", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					log.info("Result set closed");
				}
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning items list");
		return items;
	}

	@Override
	public void addItem(String name, String description, int price,
			String color, int product_id) throws DAOException {
		log.info("Adding new item");
		String sql = "INSERT INTO item (color, product) VALUES (?,?);";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, color);
			statement.setString(2, Integer.toString(product_id));
			statement.execute();
			log.info("item added");
		} catch (SQLException e) {
			log.warning("Cannot add item:" + e.getMessage());
			throw new DAOException("Cannot add item", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public Item getItem(int id) throws DAOException {
		log.info("Looking for item");
		String sql = "SELECT * FROM item WHERE iditem = ?;";
		Item item = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create item to return");
				int orderid = 0;
				try {
					orderid = Integer.parseInt(resultSet.getString("idorder"));
				} catch (Exception e) {
					orderid = 0;
				}
				item = new Item(
						Integer.parseInt(resultSet.getString("iditem")),
						Integer.parseInt(resultSet.getString("idproduct")),
						null,
						null,
						resultSet.getString("color"),
						null, orderid);
			}
		} catch (SQLException e) {
			log.warning("Cannot get item:" + e.getMessage());
			throw new DAOException("Cannot get item", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning item");
		return item;
	}

	@Override
	public void updateItem(int iditem, String color, int idorder)
			throws DAOException {
		log.info("Trying to update item");
		String sql = "UPDATE item SET color = ?, idorder = ? where iditem = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, color);
			statement.setString(2, Integer.toString(idorder));
			statement.setString(3, Integer.toString(iditem));
			statement.execute();
			log.info("item updated");
		} catch (SQLException e) {
			log.warning("Cannot update item:" + e.getMessage());
			throw new DAOException("Cannot update item", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void deleteItem(int id) throws DAOException {
		log.info("Trying to delete item");
		String sql = "DELETE FROM item WHERE iditem = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			statement.execute();
			log.info("item deleted");
		} catch (SQLException e) {
			log.warning("Cannot delete item:" + e.getMessage());
			throw new DAOException("Cannot delete item", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}
}
