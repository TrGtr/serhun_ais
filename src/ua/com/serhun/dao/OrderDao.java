package ua.com.serhun.dao;

import java.util.List;

import ua.com.serhun.domain.Order;

public interface OrderDao {

	List<Order> getAllOrders() throws DAOException;

	void addOrder(String date, String payment_method, int sum_price,
			int customer, String manager) throws DAOException;

	Order getOrder(int id) throws DAOException;

	void updateOrder(int id, String date, String payment_method, int sum_price,
			int is_confirmed, int is_delivered, String courier,
			int customer, String manager) throws DAOException;

	void deleteOrder(int id) throws DAOException;
	
	int getId(String date, int idcustomer) throws DAOException;
	
}
