package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ua.com.serhun.domain.Courier;

public class CourierDaoImpl implements CourierDao {

	private DaoFactory daoFactory = DaoFactory.getInstance();
	private static Logger log = Logger
			.getLogger(CourierDaoImpl.class.getName());

	@Override
	public List<Courier> getAllCouriers() throws DAOException {
		log.info("Getting all couriers");
		String sql = "SELECT * FROM courier;";

		List<Courier> couriers = new ArrayList<Courier>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			log.info("Adding couriers to list to return");
			while (resultSet.next()) {
				couriers.add(new Courier(Integer.parseInt(resultSet
						.getString("idcourier")), resultSet.getString("name"),
						resultSet.getString("phone")));
			}
		} catch (SQLException e) {
			log.warning("Cannot get couriers:" + e.getMessage());
			throw new DAOException("Cannot get couriers", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					log.info("Result set closed");
				}
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning couriers list");
		return couriers;
	}

	@Override
	public void addCourier(String name, String phone, String pass) throws DAOException {
		log.info("Adding new courier");
		String sql = "INSERT INTO courier (name, phone, password) VALUES (?,?,?);";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, phone);
			statement.setString(3, pass);
			statement.execute();
			log.info("courier added");
		} catch (SQLException e) {
			log.warning("Cannot add courier:" + e.getMessage());
			throw new DAOException("Cannot add courier", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public Courier getCourier(int id) throws DAOException {
		log.info("Looking for courier");
		String sql = "SELECT * FROM courier WHERE idcourier = ?;";

		Courier courier = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create courier to return");
				courier = new Courier(Integer.parseInt(resultSet
						.getString("idcourier")), resultSet.getString("name"),
						resultSet.getString("phone"),
						resultSet.getString("password"));
			}
		} catch (SQLException e) {
			log.warning("Cannot get courier:" + e.getMessage());
			throw new DAOException("Cannot get courier", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning courier");
		return courier;
	}
	
	@Override
	public Courier getCourier(String phone) throws DAOException {
		log.info("Looking for courier by phone");
		String sql = "SELECT * FROM courier WHERE phone = ?;";

		Courier courier = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, phone);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create courier to return");
				courier = new Courier(Integer.parseInt(resultSet
						.getString("idcourier")), resultSet.getString("name"),
						resultSet.getString("phone"),
						resultSet.getString("password"));
			}
		} catch (SQLException e) {
			log.warning("Cannot get courier:" + e.getMessage());
			throw new DAOException("Cannot get courier", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning courier");
		return courier;
	}

	@Override
	public void updateCourier(int id, String name, String phone)
			throws DAOException {
		log.info("Trying to update courier");
		String sql = "UPDATE COURIER SET name = ?, phone = ? where idcourier = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, phone);
			statement.setString(3, Integer.toString(id));
			statement.execute();
			log.info("courier updated");
		} catch (SQLException e) {
			log.warning("Cannot update courier:" + e.getMessage());
			throw new DAOException("Cannot update courier", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void deleteCourier(int id) throws DAOException {
		log.info("Trying to delete courier");
		String sql = "DELETE FROM courier WHERE idcourier = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			statement.execute();
			log.info("courier deleted");
		} catch (SQLException e) {
			log.warning("Cannot delete courier:" + e.getMessage());
			throw new DAOException("Cannot delete courier", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void updateCourier(int id, String name, String phone, String pass)
			throws DAOException {
		log.info("Trying to update courier(including pass)");
		String sql = "UPDATE COURIER SET name = ?, phone = ?, password = ? where idcourier = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, phone);
			statement.setString(3, pass);
			statement.setString(4, Integer.toString(id));
			statement.execute();
			log.info("courier updated");
		} catch (SQLException e) {
			log.warning("Cannot update courier:" + e.getMessage());
			throw new DAOException("Cannot update courier", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}
}
