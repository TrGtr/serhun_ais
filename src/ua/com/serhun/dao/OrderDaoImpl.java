package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ua.com.serhun.domain.Order;

public class OrderDaoImpl implements OrderDao {

	private DaoFactory daoFactory = DaoFactory.getInstance();
	private static Logger log = Logger.getLogger(OrderDaoImpl.class.getName());

	@Override
	public List<Order> getAllOrders() throws DAOException {
		log.info("Getting all orders");
		String sql = "SELECT * FROM orders;";

		List<Order> orders = new ArrayList<Order>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			log.info("Adding orders to list to return");
			while (resultSet.next()) {
				String idc = "", idm = "";
				try {
					idc = Integer.toString(Integer.parseInt(resultSet
							.getString("idcourier")));
				} catch (Exception e) {
					idc = "Not assigned";
				}

				try {
					idm = Integer.toString(Integer.parseInt(resultSet
							.getString("idmanager")));
				} catch (Exception e) {
					idm = "Not assigned";
				}

				orders.add(new Order(Integer.parseInt(resultSet
						.getString("idorder")), resultSet.getString("date"),
						resultSet.getString("payment_method"), Integer
								.parseInt(resultSet.getString("sum_price")),
						resultSet.getString("is_confirmed"), resultSet
								.getString("is_delivered"), idc, Integer
								.parseInt(resultSet.getString("idcustomer")),
						idm));
			}
		} catch (SQLException e) {
			log.warning("Cannot get orders:" + e.getMessage());
			throw new DAOException("Cannot get orders", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					log.info("Result set closed");
				}
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning orders list");
		return orders;
	}

	@Override
	public void addOrder(String date, String payment_method, int sum_price,
			int customer, String manager) throws DAOException {
		log.info("Adding new order");
		String sql = "INSERT INTO orders (date, payment_method, sum_price, is_confirmed,"
				+ " is_delivered, idcustomer, idmanager) VALUES (?,?,?,?,?,?,?);";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, date);
			statement.setString(2, payment_method);
			statement.setString(3, Integer.toString(sum_price));
			statement.setString(4, "0");
			statement.setString(5, "0");
			statement.setString(6, Integer.toString(customer));
			statement.setString(7, manager);
			statement.execute();
			log.info("order added");
		} catch (SQLException e) {
			log.warning("Cannot add order:" + e.getMessage());
			throw new DAOException("Cannot add order", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public Order getOrder(int id) throws DAOException {
		log.info("Looking for order");
		String sql = "SELECT * FROM orders WHERE idorder = ?;";

		Order order = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create order to return");
				String idc = "", idm = "";
				try {
					idc = Integer.toString(Integer.parseInt(resultSet
							.getString("idcourier")));
				} catch (Exception e) {
					idc = "0";
				}

				try {
					idm = Integer.toString(Integer.parseInt(resultSet
							.getString("idmanager")));
				} catch (Exception e) {
					idm = "0";
				}

				order = new Order(Integer.parseInt(resultSet
						.getString("idorder")), resultSet.getString("date"),
						resultSet.getString("payment_method"),
						Integer.parseInt(resultSet.getString("sum_price")),
						resultSet.getString("is_confirmed"),
						resultSet.getString("is_delivered"), idc,
						Integer.parseInt(resultSet.getString("idcustomer")),
						idm);

			}
		} catch (SQLException e) {
			log.warning("Cannot get order:" + e.getMessage());
			throw new DAOException("Cannot get order", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning order");
		return order;
	}

	@Override
	public void deleteOrder(int id) throws DAOException {
		log.info("Trying to delete order");
		String sql = "DELETE FROM orders WHERE idorder = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			statement.execute();
			log.info("order deleted");
		} catch (SQLException e) {
			log.warning("Cannot delete order:" + e.getMessage());
			throw new DAOException("Cannot delete order", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void updateOrder(int id, String date, String payment_method, int sum_price,
			int is_confirmed, int is_delivered, String courier,
			int customer, String manager)
			throws DAOException {
		log.info("Trying to update order(including pass)");
		String sql = "UPDATE orders SET date = ?, payment_method = ?, sum_price = ?,"
				+ " is_confirmed = ?, is_delivered = ?, idcourier = ?, idcustomer = ?, "
				+ "idmanager = ? where idorder = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, date);
			statement.setString(2, payment_method);
			statement.setString(3, Integer.toString(sum_price));
			statement.setString(4, Integer.toString(is_confirmed));
			statement.setString(5, Integer.toString(is_delivered));
			statement.setString(6, courier);
			statement.setString(7, Integer.toString(customer));
			statement.setString(8, manager);
			statement.setString(9,  Integer.toString(id));
			statement.execute();
			log.info("order updated");
		} catch (SQLException e) {
			log.warning("Cannot update order:" + e.getMessage());
			throw new DAOException("Cannot update order", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public int getId(String date, int idcustomer) throws DAOException {
		log.info("Looking for order");
		String sql = "SELECT * FROM orders WHERE date = ? AND idcustomer = ?;";

		Order order = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, date);
			statement.setString(2, Integer.toString(idcustomer));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create order to return");
				String idc = "", idm = "";
				try {
					idc = Integer.toString(Integer.parseInt(resultSet
							.getString("idcourier")));
				} catch (Exception e) {
					idc = "0";
				}

				try {
					idm = Integer.toString(Integer.parseInt(resultSet
							.getString("idmanager")));
				} catch (Exception e) {
					idm = "Not assigned";
				}

				order = new Order(Integer.parseInt(resultSet
						.getString("idorder")), resultSet.getString("date"),
						resultSet.getString("payment_method"),
						Integer.parseInt(resultSet.getString("sum_price")),
						resultSet.getString("is_confirmed"),
						resultSet.getString("is_delivered"), idc,
						Integer.parseInt(resultSet.getString("idcustomer")),
						idm);

			}
		} catch (SQLException e) {
			log.warning("Cannot get order:" + e.getMessage());
			throw new DAOException("Cannot get order", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning order");
		return order.getId();
	}

}
