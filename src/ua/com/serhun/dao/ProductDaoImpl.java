package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ua.com.serhun.domain.Product;

public class ProductDaoImpl implements ProductDao {

	private DaoFactory daoFactory = DaoFactory.getInstance();
	private static Logger log = Logger
			.getLogger(ProductDaoImpl.class.getName());

	@Override
	public List<Product> getAllProducts() throws DAOException {
		log.info("Getting all products");
		String sql = "SELECT * FROM product;";

		List<Product> products = new ArrayList<Product>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			log.info("Adding products to list to return");
			while (resultSet.next()) {
				products.add(new Product(Integer.parseInt(resultSet
						.getString("idproduct")), resultSet.getString("name"),
						resultSet.getString("description"), Integer
								.parseInt(resultSet.getString("price"))));
			}
		} catch (SQLException e) {
			log.warning("Cannot get products:" + e.getMessage());
			throw new DAOException("Cannot get products", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					log.info("Result set closed");
				}
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning products list");
		return products;
	}

	@Override
	public void addProduct(String name, String description, int price)
			throws DAOException {
		log.info("Adding new product");
		String sql = "INSERT INTO product (name, description, price) VALUES (?,?,?);";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, description);
			statement.setString(3, Integer.toString(price));
			statement.execute();
			log.info("product added");
		} catch (SQLException e) {
			log.warning("Cannot add product:" + e.getMessage());
			throw new DAOException("Cannot add product", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public Product getProduct(int id) throws DAOException {
		log.info("Looking for product");
		String sql = "SELECT * FROM product WHERE idproduct = ?;";
		Product product = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create product to return");
				product = new Product(Integer.parseInt(resultSet
						.getString("idproduct")), resultSet.getString("name"),
						resultSet.getString("description"),
						Integer.parseInt(resultSet.getString("price")));
			}
		} catch (SQLException e) {
			log.warning("Cannot get product:" + e.getMessage());
			throw new DAOException("Cannot get product", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning product");
		return product;
	}

	@Override
	public void updateProduct(int idproduct, String name, String description,
			int price) throws DAOException {
		log.info("Trying to update product");
		String sql = "UPDATE product SET name = ?, description = ?, price = ? where idproduct = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, description);
			statement.setString(3, Integer.toString(price));
			statement.setString(4, Integer.toString(idproduct));
			statement.execute();
			log.info("product updated");
		} catch (SQLException e) {
			log.warning("Cannot update product:" + e.getMessage());
			throw new DAOException("Cannot update product", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void deleteProduct(int id) throws DAOException {
		log.info("Trying to delete product");
		String sql = "DELETE FROM product WHERE idproduct = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			statement.execute();
			log.info("product deleted");
		} catch (SQLException e) {
			log.warning("Cannot delete product:" + e.getMessage());
			throw new DAOException("Cannot delete product", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}
}
