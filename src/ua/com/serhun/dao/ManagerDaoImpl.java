package ua.com.serhun.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import ua.com.serhun.domain.Manager;

public class ManagerDaoImpl implements ManagerDao {

	private DaoFactory daoFactory = DaoFactory.getInstance();
	private static Logger log = Logger
			.getLogger(ManagerDaoImpl.class.getName());

	@Override
	public List<Manager> getAllManagers() throws DAOException {
		log.info("Getting all managers");
		String sql = "SELECT * FROM manager;";

		List<Manager> managers = new ArrayList<Manager>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			log.info("Adding managers to list to return");
			while (resultSet.next()) {
				managers.add(new Manager(Integer.parseInt(resultSet
						.getString("idmanager")), resultSet.getString("name"),
						resultSet.getString("phone")));
			}
		} catch (SQLException e) {
			log.warning("Cannot get managers:" + e.getMessage());
			throw new DAOException("Cannot get managers", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
					log.info("Result set closed");
				}
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning managers list");
		return managers;
	}

	@Override
	public void addManager(String name, String phone, String pass) throws DAOException {
		log.info("Adding new manager");
		String sql = "INSERT INTO manager (name, phone, password) VALUES (?,?,?);";

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, phone);
			statement.setString(3, pass);
			statement.execute();
			log.info("manager added");
		} catch (SQLException e) {
			log.warning("Cannot add manager:" + e.getMessage());
			throw new DAOException("Cannot add manager", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public Manager getManager(int id) throws DAOException {
		log.info("Looking for manager");
		String sql = "SELECT * FROM manager WHERE idmanager = ?;";

		Manager manager = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create manager to return");
				manager = new Manager(Integer.parseInt(resultSet
						.getString("idmanager")), resultSet.getString("name"),
						resultSet.getString("phone"),
						resultSet.getString("password"));
			}
		} catch (SQLException e) {
			log.warning("Cannot get manager:" + e.getMessage());
			throw new DAOException("Cannot get manager", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning manager");
		return manager;
	}
	
	@Override
	public Manager getManager(String phone) throws DAOException {
		log.info("Looking for manager by phone");
		String sql = "SELECT * FROM manager WHERE phone = ?;";

		Manager manager = null;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, phone);
			log.info("Get result set");
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				log.info("Create manager to return");
				manager = new Manager(Integer.parseInt(resultSet
						.getString("idmanager")), resultSet.getString("name"),
						resultSet.getString("phone"),
						resultSet.getString("password"));
			}
		} catch (SQLException e) {
			log.warning("Cannot get manager:" + e.getMessage());
			throw new DAOException("Cannot get manager", e);
		} finally {
			try {
				resultSet.close();
				log.info("Result set closed");
			} catch (SQLException e1) {
				log.warning("Cannot close result set: " + e1.getMessage());
			}
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
		log.info("Returning manager");
		return manager;
	}

	@Override
	public void updateManager(int id, String name, String phone)
			throws DAOException {
		log.info("Trying to update manager");
		String sql = "UPDATE manager SET name = ?, phone = ? WHERE idmanager = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, phone);
			statement.setString(3, Integer.toString(id));
			statement.execute();
			log.info("manager updated");
		} catch (SQLException e) {
			log.warning("Cannot update manager:" + e.getMessage());
			throw new DAOException("Cannot update manager", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void deleteManager(int id) throws DAOException {
		log.info("Trying to delete manager");
		String sql = "DELETE FROM manager WHERE idmanager = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, Integer.toString(id));
			statement.execute();
			log.info("manager deleted");
		} catch (SQLException e) {
			log.warning("Cannot delete manager:" + e.getMessage());
			throw new DAOException("Cannot delete manager", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}

	@Override
	public void updateManager(int id, String name, String phone, String pass)
			throws DAOException {
		log.info("Trying to update manager(including pass)");
		String sql = "UPDATE manager SET name = ?, phone = ?, password = ? where idmanager = ?;";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			log.info("Open connection");
			connection = daoFactory.getConnection();
			log.info("Create prepared statement");
			statement = connection.prepareStatement(sql);
			statement.setString(1, name);
			statement.setString(2, phone);
			statement.setString(3, pass);
			statement.setString(4, Integer.toString(id));
			statement.execute();
			log.info("manager updated");
		} catch (SQLException e) {
			log.warning("Cannot update manager:" + e.getMessage());
			throw new DAOException("Cannot update manager", e);
		} finally {
			try {
				statement.close();
				log.info("Statement closed");
			} catch (SQLException e1) {
				log.warning("Cannot close statement: " + e1.getMessage());
			}
			try {
				connection.close();
				log.info("Connection closed");
			} catch (SQLException e2) {
				log.warning("Cannot close result set: " + e2.getMessage());
			}
		}
	}
}
