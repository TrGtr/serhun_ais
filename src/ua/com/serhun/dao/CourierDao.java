package ua.com.serhun.dao;

import java.util.List;

import ua.com.serhun.domain.Courier;

public interface CourierDao {

	List<Courier> getAllCouriers() throws DAOException;

	void addCourier(String name, String phone, String pass)
			throws DAOException;

	Courier getCourier(int id) throws DAOException;
	
	Courier getCourier(String phone) throws DAOException;

	void updateCourier(int id, String name, String phone)
			throws DAOException;
	
	void updateCourier(int id, String name, String phone, String pass)
			throws DAOException;

	void deleteCourier(int id) throws DAOException;

}
