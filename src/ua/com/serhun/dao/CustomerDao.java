package ua.com.serhun.dao;

import java.util.List;

import ua.com.serhun.domain.Customer;

public interface CustomerDao {

	List<Customer> getAllCustomers() throws DAOException;

	void addCustomer(String name, String email, String phone, String address, String pass)
			throws DAOException;

	Customer getCustomer(int id) throws DAOException;
	
	Customer getCustomer(String phone) throws DAOException;

	void updateCustomer(int id, String name, String email, String phone, String address, String pass)
			throws DAOException;
	
	void updateCustomer(int id, String name, String email, String phone, String address)
			throws DAOException;

	void deleteCustomer(int id) throws DAOException;

}
