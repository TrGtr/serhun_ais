package ua.com.serhun.dao;

import java.util.List;

import ua.com.serhun.domain.Item;

public interface ItemDao {

	List<Item> getAllItems() throws DAOException;

	void addItem(String name, String description, int price, String color, int product_id)
			throws DAOException;

	Item getItem(int id) throws DAOException;

	void updateItem(int iditem, String color, int idorder)
			throws DAOException;

	void deleteItem(int id) throws DAOException;

}
