package ua.com.serhun.domain;

public class Item implements Comparable<Item> {

	private int iditem;
	private int idproduct;
	private String name;
	private String description;
	private String color;
	private String price;
	private int idorder;

	public Item(int iditem, int idproduct, String name, String description,
			String color, String price, int idorder) {
		super();
		this.iditem = iditem;
		this.idproduct = idproduct;
		this.name = name;
		this.description = description;
		this.color = color;
		this.price = price;
		this.idorder = idorder;
	}
	
	public Item(int iditem, String color, int idorder) {
		super();
		this.iditem = iditem;
		this.color = color;
		this.idorder = idorder;
	}

	public int getIditem() {
		return iditem;
	}

	public void setIditem(int iditem) {
		this.iditem = iditem;
	}

	public int getIdproduct() {
		return idproduct;
	}

	public void setIdproduct(int idproduct) {
		this.idproduct = idproduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getIdorder() {
		return idorder;
	}

	public void setIdorder(int idorder) {
		this.idorder = idorder;
	}

	@Override
	public int compareTo(Item o) {
		if (this.getIditem() != o.getIditem())
			return -1;
		if (this.getIdproduct() != o.getIdproduct())
			return -1;
		if (!this.getName().equals(o.getName()))
			return -1;
		if (!this.getDescription().equals(o.getDescription()))
			return -1;
		if (this.getColor() != null) {
			if (!this.getColor().equals(o.getColor()))
				return -1;
		}
		if (!this.getPrice().equals(o.getPrice()))
			return -1;

		if (this.getIdorder() != o.getIdorder())
			return -1;

		return 0;
	}

}
