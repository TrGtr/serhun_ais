package ua.com.serhun.domain;

public class Customer implements Comparable<Customer> {

	private int id;
	private String name;
	private String email;
	private String phone;
	private String address;
	private String pass;

	public Customer(int id, String name, String email, String phone,
			String address) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
	}

	public Customer(int id, String name, String email, String phone,
			String address, String pass) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.pass = pass;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Customer o) {
		if (this.getId() != o.getId()) return -1;
		if (!this.getName().equals(o.getName())) return -1;
		if (!this.getEmail().equals(o.getEmail())) return -1;
		if (!this.getPhone().equals(o.getPhone())) return -1;
		if (!this.getAddress().equals(getAddress())) return -1;
		return 0;
	}

}
