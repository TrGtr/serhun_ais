package ua.com.serhun.domain;

public class Product implements Comparable<Product> {

	private int id;
	private String name;
	private String description;
	private int price;

	public Product(int id, String name, String description, int price) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public int compareTo(Product o) {
		if (this.getId() != o.getId())
			return -1;
		if (!this.getName().equals(o.getName()))
			return -1;
		if (!this.getDescription().equals(o.getDescription()))
			return -1;
		if (this.getPrice() != o.getPrice())
			return -1;
		return 0;
	}

}
