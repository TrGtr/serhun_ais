package ua.com.serhun.domain;

public class Courier implements Comparable<Courier>{

	private int id;
	private String name;
	private String phone;
	private String pass;

	public Courier(int id, String name, String phone) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
	}

	public Courier(int id, String name, String phone, String pass) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.pass = pass;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(Courier o) {
		if (this.getId() != o.getId()) return -1;
		if (!this.getName().equals(o.getName())) return -1;
		if (!this.getPhone().equals(o.getPhone())) return -1;
		return 0;
	}

}
