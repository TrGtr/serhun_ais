package ua.com.serhun.domain;

public class Order {

	private int id;
	private String date;
	private String payment_method;
	private int sum_price;
	private String is_confirmed;
	private String is_delivered;
	private String courier;
	private int customer;
	private String manager;

	public Order(int id, String date, String payment_method, int sum_price,
			String is_confirmed, String is_delivered, String courier,
			int customer, String manager) {
		super();
		this.id = id;
		this.date = date;
		this.payment_method = payment_method;
		this.sum_price = sum_price;
		this.is_confirmed = is_confirmed;
		this.is_delivered = is_delivered;
		this.courier = courier;
		this.customer = customer;
		this.manager = manager;
	}

	public Order(int id, String date, String payment_method, int sum_price,
			String is_confirmed, String is_delivered, int customer,
			String manager) {
		super();
		this.id = id;
		this.date = date;
		this.payment_method = payment_method;
		this.sum_price = sum_price;
		this.is_confirmed = is_confirmed;
		this.is_delivered = is_delivered;
		this.customer = customer;
		this.manager = manager;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public int getSum_price() {
		return sum_price;
	}

	public void setSum_price(int sum_price) {
		this.sum_price = sum_price;
	}

	public String getIs_confirmed() {
		return is_confirmed;
	}

	public void setIs_confirmed(String is_confirmed) {
		this.is_confirmed = is_confirmed;
	}

	public String getIs_delivered() {
		return is_delivered;
	}

	public void setIs_delivered(String is_delivered) {
		this.is_delivered = is_delivered;
	}

	public String getCourier() {
		return courier;
	}

	public void setCourier(String courier) {
		this.courier = courier;
	}

	public int getCustomer() {
		return customer;
	}

	public void setCustomer(int customer) {
		this.customer = customer;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

}
