package ua.com.serhun.domain;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encrypter {
	
	/**
	 * 
	 * @param str String to process
	 * @return encrypted string
	 * @throws NoSuchAlgorithmException
	 */
	public String sha256(String str) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(str.getBytes());		 
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
		
		
		return sb.toString();
	}
}
