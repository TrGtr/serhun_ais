package ua.com.serhun.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.com.serhun.dao.CourierDao;
import ua.com.serhun.dao.CourierDaoImpl;
import ua.com.serhun.dao.CustomerDao;
import ua.com.serhun.dao.CustomerDaoImpl;
import ua.com.serhun.dao.ManagerDao;
import ua.com.serhun.dao.ManagerDaoImpl;
import ua.com.serhun.domain.Courier;
import ua.com.serhun.domain.Customer;
import ua.com.serhun.domain.Encrypter;
import ua.com.serhun.domain.Manager;

/**
 * Servlet implementation class AuthServlet
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("phone") != null) {
			session.setAttribute("phone", null);
			response.sendRedirect("register.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("phone") != null) {
			session.setAttribute("phone", null);
		}

		String phone = request.getParameter("phone");
		String password = request.getParameter("password");

		String hPass = "";

		Encrypter encr = new Encrypter();

		CustomerDao sdao = new CustomerDaoImpl();
		try {
			hPass = encr.sha256(password);
			if (sdao.getCustomer(phone) != null) {
				response.sendRedirect("register.jsp");
			}
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String address = request.getParameter("address");
			
			
			sdao.addCustomer(name, email, phone, address, hPass);

			session.setAttribute("phone", phone);
			session.setAttribute("role", "customer");

			response.sendRedirect("items.jsp");
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("register.jsp");
		}

	}
}
