package ua.com.serhun.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.com.serhun.dao.CourierDao;
import ua.com.serhun.dao.CourierDaoImpl;
import ua.com.serhun.dao.CustomerDao;
import ua.com.serhun.dao.CustomerDaoImpl;
import ua.com.serhun.dao.ManagerDao;
import ua.com.serhun.dao.ManagerDaoImpl;
import ua.com.serhun.domain.Courier;
import ua.com.serhun.domain.Customer;
import ua.com.serhun.domain.Encrypter;
import ua.com.serhun.domain.Manager;

/**
 * Servlet implementation class AuthServlet
 */
@WebServlet("/auth")
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("phone") != null) {
			session.setAttribute("phone", null);
			response.sendRedirect("authorize.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("phone") != null) {
			session.setAttribute("phone", null);
			response.sendRedirect("authorize.jsp");
		} else {
			System.out.println("1");
			String phone = request.getParameter("phone");
			String password = request.getParameter("password");

			String hPass = "";

			Encrypter encr = new Encrypter();
			try {
				hPass = encr.sha256(password);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			if (request.getParameter("role").equals("customer")) {
				CustomerDao sdao = new CustomerDaoImpl();
				try {
					Customer cust = sdao.getCustomer(phone);
					if (cust.getPass().equals(hPass)) {
						session.setAttribute("phone", phone);
						session.setAttribute("role", "customer");
						session.setAttribute("id", sdao.getCustomer(phone).getId());
						response.sendRedirect("items.jsp");
					}
				} catch (Exception e) {
					response.sendRedirect("authorize.jsp");
				}
			} else if (request.getParameter("role").equals("courier")) {
				CourierDao sdao = new CourierDaoImpl();
				try {
					Courier cust = sdao.getCourier(phone);
					if (cust.getPass().equals(hPass)) {
						session.setAttribute("phone", phone);
						session.setAttribute("role", "courier");

						response.sendRedirect("items.jsp");
					}
				} catch (Exception e) {
					response.sendRedirect("authorize.jsp");
				}
			} else if (request.getParameter("role").equals("manager")) {
				ManagerDao sdao = new ManagerDaoImpl();
				try {
					Manager cust = sdao.getManager(phone);
					if (cust.getPass().equals(hPass)) {
						session.setAttribute("phone", phone);
						session.setAttribute("role", "manager");

						response.sendRedirect("items.jsp");
					}
				} catch (Exception e) {
					response.sendRedirect("authorize.jsp");
				}
			}
		}
	}
}
