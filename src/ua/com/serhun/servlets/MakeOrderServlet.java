package ua.com.serhun.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.com.serhun.dao.CustomerDao;
import ua.com.serhun.dao.CustomerDaoImpl;
import ua.com.serhun.dao.DAOException;
import ua.com.serhun.dao.ItemDao;
import ua.com.serhun.dao.ItemDaoImpl;
import ua.com.serhun.dao.OrderDao;
import ua.com.serhun.dao.OrderDaoImpl;
import ua.com.serhun.dao.ProductDao;
import ua.com.serhun.dao.ProductDaoImpl;
import ua.com.serhun.domain.Item;

/**
 * Servlet implementation class MakeOrderServlet
 */
@WebServlet("/makeOrder")
public class MakeOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MakeOrderServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("phone") != null
				&& session.getAttribute("role").equals("customer")) {
			String items = request.getParameter("orderItems");
			List<Integer> ids = new ArrayList<Integer>();

			while (items.indexOf(';') != -1) {
				String id;
				id = items.substring(0, items.indexOf(';'));
				if (items.indexOf(';') != -1) {
					items = items.substring(items.indexOf(';') + 1);
				}
				ids.add(Integer.parseInt(id));
			}
			OrderDao odao = new OrderDaoImpl();
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); // 2014/08/06 15:59:48
			CustomerDao cdao = new CustomerDaoImpl();
			ItemDao idao = new ItemDaoImpl();
			ProductDao pdao = new ProductDaoImpl();

			int id;
			try {
				id = cdao.getCustomer(session.getAttribute("phone").toString())
						.getId();
				String dateStr = dateFormat.format(date);
				odao.addOrder(dateStr, request.getParameter("pmethod"), 0, id,
						"2");
				int oId = odao.getId(dateStr, id);
				int price = 0;
				for (Integer num : ids) {
					Item item = idao.getItem(num);
					idao.updateItem(item.getIditem(), item.getColor(), oId);
					price += pdao.getProduct(item.getIdproduct()).getPrice();
				}
				odao.updateOrder(oId, dateFormat.format(date),
						request.getParameter("pmethod"), price, 0, 0,
						"0", id, "2");
				response.sendRedirect("orders.jsp");
			} catch (DAOException e) {
				e.printStackTrace();
			}
		} else {
			response.sendRedirect("items.jsp");
		}
	}

}
