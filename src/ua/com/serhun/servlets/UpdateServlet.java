package ua.com.serhun.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.com.serhun.dao.CourierDao;
import ua.com.serhun.dao.CourierDaoImpl;
import ua.com.serhun.dao.CustomerDao;
import ua.com.serhun.dao.CustomerDaoImpl;
import ua.com.serhun.dao.DAOException;
import ua.com.serhun.dao.ItemDao;
import ua.com.serhun.dao.ItemDaoImpl;
import ua.com.serhun.dao.ManagerDao;
import ua.com.serhun.dao.ManagerDaoImpl;
import ua.com.serhun.dao.OrderDao;
import ua.com.serhun.dao.OrderDaoImpl;
import ua.com.serhun.dao.ProductDao;
import ua.com.serhun.dao.ProductDaoImpl;
import ua.com.serhun.domain.Courier;
import ua.com.serhun.domain.Customer;
import ua.com.serhun.domain.Item;
import ua.com.serhun.domain.Manager;
import ua.com.serhun.domain.Product;

/**
 * Servlet implementation class update
 */
@WebServlet("/update")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("couriers.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("from") != null) {
			String from = request.getParameter("from");
			if (request.getParameter("action").equals("del")) {
				if (from.equals("couriers")) {
					CourierDao cdao = new CourierDaoImpl();
					try {
						cdao.deleteCourier(Integer.parseInt(request
								.getParameter("id")));
					} catch (NumberFormatException | DAOException e) {
						;
					}
					response.sendRedirect(from + ".jsp");
				} else if (from.equals("customers")) {
					CustomerDao cdao = new CustomerDaoImpl();
					try {
						cdao.deleteCustomer(Integer.parseInt(request
								.getParameter("id")));
					} catch (NumberFormatException | DAOException e) {
						;
					}
					response.sendRedirect(from + ".jsp");
				} else if (from.equals("items")) {
					ItemDao cdao = new ItemDaoImpl();
					try {
						cdao.deleteItem(Integer.parseInt(request
								.getParameter("id")));
					} catch (NumberFormatException | DAOException e) {
						;
					}
					response.sendRedirect(from + ".jsp");
				} else if (from.equals("managers")) {
					ManagerDao cdao = new ManagerDaoImpl();
					try {
						cdao.deleteManager(Integer.parseInt(request
								.getParameter("id")));
					} catch (NumberFormatException | DAOException e) {
						;
					}
					response.sendRedirect(from + ".jsp");
				} else if (from.equals("orders")) {
					OrderDao cdao = new OrderDaoImpl();
					try {
						cdao.deleteOrder(Integer.parseInt(request
								.getParameter("id")));
					} catch (NumberFormatException | DAOException e) {
						;
					}
					response.sendRedirect(from + ".jsp");
				}
			} else {

				// COURIERS
				if (from.equals("couriers")) {
					int size = Integer.parseInt(request.getParameter("count"));
					List<Courier> couriers = new LinkedList<Courier>();
					if (size > 0) {
						for (int i = 1; i <= size; ++i) {
							couriers.add(new Courier(Integer.parseInt(request
									.getParameter(i + "id")), request
									.getParameter(i + "name"), request
									.getParameter(i + "phone")));
						}
						CourierDao cdao = new CourierDaoImpl();
						List<Courier> couriersDef = new LinkedList<Courier>();
						try {
							couriersDef.addAll(cdao.getAllCouriers());
						} catch (DAOException e) {
							e.printStackTrace();
						}
						if (couriersDef.size() > 0) {
							for (int i = 0; i < couriersDef.size(); ++i) {
								if (couriers.get(i).getName().length() > 3
										&& couriers.get(i).getPhone().length() > 3) {
									if (couriers.get(i).compareTo(
											couriersDef.get(i)) != 0) {
										try {
											cdao.updateCourier(couriers.get(i)
													.getId(), couriers.get(i)
													.getName(), couriers.get(i)
													.getPhone());
										} catch (DAOException e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					response.sendRedirect(from + ".jsp");
				}
				// --------
				// Managers
				else if (from.equals("managers")) {
					int size = Integer.parseInt(request.getParameter("count"));
					List<Manager> managers = new LinkedList<Manager>();
					if (size > 0) {
						for (int i = 1; i <= size; ++i) {
							managers.add(new Manager(Integer.parseInt(request
									.getParameter(i + "id")), request
									.getParameter(i + "name"), request
									.getParameter(i + "phone")));
						}
						ManagerDao cdao = new ManagerDaoImpl();
						List<Manager> managersDef = new LinkedList<Manager>();
						try {
							managersDef.addAll(cdao.getAllManagers());
						} catch (DAOException e) {
							e.printStackTrace();
						}
						if (managersDef.size() > 0) {
							for (int i = 0; i < managersDef.size(); ++i) {
								if (managers.get(i).getName().length() > 3
										&& managers.get(i).getPhone().length() > 3) {
									if (managers.get(i).compareTo(
											managersDef.get(i)) != 0) {
										try {
											cdao.updateManager(managers.get(i)
													.getId(), managers.get(i)
													.getName(), managers.get(i)
													.getPhone());
										} catch (DAOException e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					response.sendRedirect(from + ".jsp");
				}
				// --------
				// Customers
				else if (from.equals("customers")) {
					int size = Integer.parseInt(request.getParameter("count"));
					List<Customer> customers = new LinkedList<Customer>();
					if (size > 0) {
						System.out.println(size);
						for (int i = 1; i <= size; ++i) {
							customers.add(new Customer(Integer.parseInt(request
									.getParameter(i + "id")), request
									.getParameter(i + "name"), request
									.getParameter(i + "email"), request
									.getParameter(i + "phone"), request
									.getParameter(i + "address")));
						}
						CustomerDao cdao = new CustomerDaoImpl();
						List<Customer> customersDef = new LinkedList<Customer>();
						try {
							customersDef.addAll(cdao.getAllCustomers());
						} catch (DAOException e) {
							e.printStackTrace();
						}
						if (customersDef.size() > 0) {
							for (int i = 0; i < customersDef.size(); ++i) {
								if (customers.get(i).getName().length() > 3
										&& customers.get(i).getPhone().length() > 3
										&& customers.get(i).getEmail().length() > 3
										&& customers.get(i).getAddress()
												.length() > 3) {
									if (customers.get(i).compareTo(
											customersDef.get(i)) != 0) {
										try {
											cdao.updateCustomer(
													customers.get(i).getId(),
													customers.get(i).getName(),
													customers.get(i).getEmail(),
													customers.get(i).getPhone(),
													customers.get(i)
															.getAddress());
										} catch (DAOException e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					response.sendRedirect(from + ".jsp");
				}
				// --------
				else if (from.equals("items")) {
					int size = Integer.parseInt(request.getParameter("count"));
					List<Item> items = new LinkedList<Item>();
					if (size > 0) {
						for (int i = 1; i <= size; ++i) {
							items.add(new Item(Integer.parseInt(request
									.getParameter(i + "iditem")), request
									.getParameter(i + "color"), Integer
									.parseInt(request.getParameter(i
											+ "idorder"))));
							System.out.println(items.get(i - 1).getName());
						}
						ItemDao cdao = new ItemDaoImpl();
						List<Item> itemsDef = new LinkedList<Item>();
						try {
							itemsDef.addAll(cdao.getAllItems());
						} catch (DAOException e) {
							e.printStackTrace();
						}
						if (itemsDef.size() > 0) {
							for (int i = 0; i < itemsDef.size(); ++i) {
								if (items.get(i).getColor().length() > 3) {
									if (items.get(i).compareTo(itemsDef.get(i)) != 0) {
										try {
											cdao.updateItem(items.get(i)
													.getIditem(), items.get(i)
													.getColor(), items.get(i)
													.getIdorder());
										} catch (DAOException e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					response.sendRedirect(from + ".jsp");
				}
				// Products
				else if (from.equals("products")) {
					int size = Integer.parseInt(request.getParameter("count"));
					List<Product> prods = new LinkedList<Product>();
					if (size > 0) {
						for (int i = 1; i <= size; ++i) {
							prods.add(new Product(Integer.parseInt(request
									.getParameter(i + "idproduct")), request
									.getParameter(i + "name"), request
									.getParameter(i + "description"),
									Integer.parseInt(request.getParameter(i
											+ "price"))));
							System.out.println(prods.get(i - 1).getName());
						}
						ProductDao cdao = new ProductDaoImpl();
						List<Product> prodsDef = new LinkedList<Product>();
						try {
							prodsDef.addAll(cdao.getAllProducts());
						} catch (DAOException e) {
							e.printStackTrace();
						}
						if (prodsDef.size() > 0) {
							for (int i = 0; i < prodsDef.size(); ++i) {
								if (prods.get(i).getName().length() > 3) {
									if (prods.get(i).compareTo(prodsDef.get(i)) != 0) {
										try {
											cdao.updateProduct(prods.get(i)
													.getId(), prods.get(i)
													.getName(), prods.get(i)
													.getDescription(), prods
													.get(i).getPrice());
										} catch (DAOException e) {
											e.printStackTrace();
										}
									}
								}
							}
						}
					}
					response.sendRedirect(from + ".jsp");
					// --------

				}
			}
		}
	}
}
