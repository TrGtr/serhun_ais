<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/reset.css">
<!-- CSS reset -->
<link rel="stylesheet" href="css/style.css">
<!-- Resource style -->
<script src="js/modernizr.js"></script>
<!-- Modernizr -->

<title>AIS</title>
</head>
<body>
	<header class="cd-main-header"> <a href="#0" class="cd-logo"><img
		src="img/cd-logo.svg" alt="Logo"></a>

	 <a href="#0" class="cd-nav-trigger">Menu<span></span></a>

	<nav class="cd-nav">
	<ul class="cd-top-nav">
		<li class="has-children account"><a href="#0">        <%
        HttpSession se = request.getSession();
		if (se.getAttribute("phone")==null) {
            response.sendRedirect("authorize.jsp");
        }
        out.println(se.getAttribute("role")+"("+se.getAttribute("phone")+")");
        if (!se.getAttribute("role").equals("manager") && !se.getAttribute("role").equals("courier")) {
            response.sendRedirect("items.jsp");
        }
        %>  </a>

			<ul>
				<li><a href="auth">Logout</a></li>
			</ul></li>
	</ul>
	</nav> </header>
	<!-- .cd-main-header -->

	<main class="cd-main-content"> <nav class="cd-side-nav">
<!-- 	<ul>
		<li class="cd-label">Main</li>
		<li class="has-children notifications"><a href="#0">Notifications<span
				class="count">3</span></a>

			<ul>
				<li><a href="#0">All Notifications</a></li>
			 </ul></li>

	</ul>
-->
	<ul>
		<li class="cd-label">Menu</li>
		<li class="has-children overview active"><a href="#0" id='overvw'>Overview</a>

			<ul>
			   <%if (se.getAttribute("role").equals("customer")) {
                out.println("<li><a href='items.jsp'>Items</a></li>");
                out.println("<li><a href='orders.jsp'>Orders</a></li>");
            }
            else {
                out.println("<li><a href='products.jsp'>Products</a></li>");
                out.println("<li><a href='items.jsp'>Items</a></li>");
                out.println("<li><a href='orders.jsp'>Orders</a></li>");
                out.println("<li><a href='couriers.jsp'>Couriers</a></li>");
                out.println("<li><a href='managers.jsp'>Managers</a></li>");
                out.println("<li><a href='customers.jsp'>Customers</a></li>");
            }
            %>
			</ul></li>

	</ul>

	<ul>
		<li class="cd-label">Actions</li>
		<li class="action-btn" id='edbtn'><a href="#0">Edit mode</a></li>
		<li><br></li>
		<li class="action-btn hidden" id='savebtn'><center>
				<input type="submit" form="myform" value="Save changes">
			</center></li>
	</ul>
	</nav>

	<div class="content-wrapper">
		<h1>Products</h1>
		<form id="myform" action="update" method="POST">
			<%
				ua.com.serhun.dao.ProductDao bdao = new ua.com.serhun.dao.ProductDaoImpl();
				try {
					out.println("<table id=\"productsTable\" class=\"input-list style-1 clearfix\">");
					out.println("<tr>");
					out.println("<th>product ID</th>");
					out.println("<th>name</th>");
					out.println("<th>description</th>");
					out.println("<th>price</th>");
					out.println("</tr>");
					java.util.List<ua.com.serhun.domain.Product> prods = new java.util.LinkedList<ua.com.serhun.domain.Product>();
					prods.addAll(bdao.getAllProducts());
					int i = 0;
					for (ua.com.serhun.domain.Product prod : prods) {
						++i;
						out.print("<tr>");
						out.println("<td>" + prod.getId() + "</td>");
						out.println("<input type='hidden' name='" + i
								+ "idproduct' value='" + prod.getId() + "'>");
						out.println("<td class='cb' id='" + i + "name" + "'>" + prod.getName()
								+ "</td>");
						out.println("<td class='cb' id='" + i + "description" + "'>"
								+ prod.getDescription() + "</td>");
						out.println("<td class='cb' id='" + i + "price" + "'>"
								+ prod.getPrice() + "</td>");
						out.print("</tr>");

					}
					out.println("</table>");
					out.println("<input type='hidden' name='count' value='"
							+ prods.size() + "'>");
					out.println("<input type='hidden' name='from' value='products'>");
					out.println("<input type='hidden' id='action' name='action' value='upd'>");
					out.println("<input type='hidden' id='id' name='id' value=''>");
				} catch (ua.com.serhun.dao.DAOException e) {
					out.println("An Error occured when connecting to DataBase: "
							+ e.getLocalizedMessage());
				}
			%>
		</form>
	</div>
	<!-- .content-wrapper --> </main>
	<!-- .cd-main-content -->
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.menu-aim.js"></script>
	<script src="js/main.js"></script>
	<!-- Resource jQuery -->
</body>
</html>