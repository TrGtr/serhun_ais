<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/reset.css">
<!-- CSS reset -->
<link rel="stylesheet" href="css/style.css">
<!-- Resource style -->
<script src="js/modernizr.js"></script>
<!-- Modernizr -->

<title>AIS</title>
</head>
<body>
	<header class="cd-main-header"> <a href="#0" class="cd-logo"><img
		src="img/cd-logo.svg" alt="Logo"></a> <a href="#0"
		class="cd-nav-trigger">Menu<span></span></a> <nav class="cd-nav">
	<ul class="cd-top-nav">
		<li class="has-children account"><a href="#0"> <%
 	HttpSession se = request.getSession();
 	if (se.getAttribute("phone") == null) {
 		response.sendRedirect("authorize.jsp");
 	}
 	out.println(se.getAttribute("role") + "("
 			+ se.getAttribute("phone") + ")");
 %>
		</a>

			<ul>
				<li><a href="auth">Logout</a></li>
			</ul></li>
	</ul>
	</nav> </header>
	<!-- .cd-main-header -->

	<main class="cd-main-content"> <nav class="cd-side-nav">

	<ul>
		<li class="cd-label">Menu</li>
		<li class="has-children overview active"><a href="#0" id='overvw'>Overview</a>

			<ul>
				<%
					if (se.getAttribute("role").equals("customer")) {
						out.println("<li><a href='items.jsp'>Items</a></li>");
						out.println("<li><a href='orders.jsp'>Orders</a></li>");
					} else {
						out.println("<li><a href='products.jsp'>Products</a></li>");
						out.println("<li><a href='items.jsp'>Items</a></li>");
						out.println("<li><a href='orders.jsp'>Orders</a></li>");
						out.println("<li><a href='couriers.jsp'>Couriers</a></li>");
						out.println("<li><a href='managers.jsp'>Managers</a></li>");
						out.println("<li><a href='customers.jsp'>Customers</a></li>");
					}
				%>
			</ul></li>
	</ul>

	<ul>


	</ul>
	</nav>

	<div class="content-wrapper">
		<h1>Orders</h1>
		<form id="myform" action="update" method="POST">
			<%
				ua.com.serhun.dao.OrderDao bdao = new ua.com.serhun.dao.OrderDaoImpl();
				try {
					out.println("<table id=\"orderTable\" class=\"input-list style-1 clearfix\">");
					out.println("<tr>");
					out.println("<th>order ID</th>");
					out.println("<th>date</th>");
					out.println("<th>payment method</th>");
					out.println("<th>sum price</th>");
					out.println("<th>is confirmed</th>");
					out.println("<th>is delivered</th>");				
					if (!se.getAttribute("role").equals("customer")) {
						out.println("<th>idcourier</th>");
	                    out.println("<th>idcustomer</th>");
	                    out.println("<th>idmanager</th>");
						out.println("<td>row action</td>");
					} else {
						out.println("<th>courier phone</th>");
                        out.println("<th>manager phone</th>");
					}
					out.println("</tr>");
					for (ua.com.serhun.domain.Order order : bdao.getAllOrders()) {
						if (order.getId() > 0) {
						    
							out.print("<tr>");
							if (se.getAttribute("role").equals("customer")) {
								if (order.getCustomer() == Integer.parseInt(se
										.getAttribute("id").toString())) {
									out.println("<td>" + order.getId() + "</td>");
									out.println("<td class='cb'>" + order.getDate()
											+ "</td>");
									out.println("<td class='cb'>"
											+ order.getPayment_method() + "</td>");
									out.println("<td class='cb'>"
											+ order.getSum_price() + "</td>");
									out.println("<td class='cb'>"
											+ order.getIs_confirmed() + "</td>");
									out.println("<td class='cb'>"
											+ order.getIs_delivered() + "</td>");
									
									ua.com.serhun.dao.CourierDao cdao = new ua.com.serhun.dao.CourierDaoImpl();
									ua.com.serhun.dao.ManagerDao mdao = new ua.com.serhun.dao.ManagerDaoImpl();
									String phoneC = "";
									String phoneM = "";
									try {
										phoneC = "" + cdao.getCourier(Integer.parseInt(order.getCourier())).getPhone();
										
	                                    phoneM = "" + mdao.getManager(Integer.parseInt(order.getManager())).getPhone();
									} catch (Exception e) {
										e.printStackTrace();
									}
									
									out.println("<td class='cb'>"
											+ phoneC + "</td>");
									out.println("<td class='cb'>"
											+ phoneM + "</td>");
								}
							} else {
								out.println("<td>" + order.getId() + "</td>");
								out.println("<td class='cb'>" + order.getDate()
										+ "</td>");
								out.println("<td class='cb'>"
										+ order.getPayment_method() + "</td>");
								out.println("<td class='cb'>"
										+ order.getSum_price() + "</td>");
								out.println("<td class='cb'>"
										+ order.getIs_confirmed() + "</td>");
								out.println("<td class='cb'>"
										+ order.getIs_delivered() + "</td>");
								out.println("<td class='cb'>" + order.getCourier()
										+ "</td>");
								out.println("<td class='cb'>" + order.getCustomer()
										+ "</td>");
								out.println("<td class='cb'>" + order.getManager()
										+ "</td>");
								out.println("<td width='1%'><input type='submit' class='delbtn' id='"
										+ order.getId()
										+ "' value='delete' form='myform' value='delete'>"
										+ "</td>");
							}

							out.print("</tr>");
						}
					}
					out.println("</table>");
					out.println("<input type='hidden' id='action' name='action' value='upd'>");
					out.println("<input type='hidden' id='id' name='id' value=''>");
				} catch (ua.com.serhun.dao.DAOException e) {
					out.println("An Error occured when connecting to DataBase: "
							+ e.getLocalizedMessage());
				}
			%>
		</form>
	</div>
	<!-- .content-wrapper --> </main>
	<!-- .cd-main-content -->
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.menu-aim.js"></script>
	<script src="js/main.js"></script>
	<!-- Resource jQuery -->
</body>
</html>