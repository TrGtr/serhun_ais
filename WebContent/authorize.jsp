<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html5/loose.dtd">
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/reset.css">
<!-- CSS reset -->
<link rel="stylesheet" href="css/style.css">
<!-- Resource style -->
<script src="js/modernizr.js"></script>
<!-- Modernizr -->

<title>AIS</title>
</head>
<body>
	<header class="cd-main-header"> <a href="#0" class="cd-logo"><img
		src="img/cd-logo.svg" alt="Logo"></a> <!-- cd-search --> <a
		href="#0" class="cd-nav-trigger">Menu<span></span></a> <nav
		class="cd-nav">
	<ul class="cd-top-nav">
		<li class="has-children account"><a href="#0">Account
		</a>
		<ul>
                <li><a href="register.jsp">Register</a></li>
            </ul></li>	

	</ul>
	</nav> </header>
	<!-- .cd-main-header -->

	<main class="cd-main-content"> <nav class="cd-side-nav">
	<ul>
		<li class="cd-label">Need authorization</li>

	</ul>

	</nav>

	<div class="content-wrapper">
		<h1>Sign In</h1>
		<form id="myform" action="auth" method="POST">
			<center>				
				
                Phone: <input type='text' name='phone' value=''>
                <br><br>Password: <input type="password" name='password' value=''>
                <br><br><select name='role'>
                    <option>customer</option>
                    <option>manager</option>
                    <option>courier</option>
                </select>
                <br><br><input type="submit" form="myform" value="Log in">
			</center>
			<%
				
			%>
		</form>
	</div>
	<!-- .content-wrapper --> </main>
	<!-- .cd-main-content -->
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.menu-aim.js"></script>
	<script src="js/main.js"></script>
	<!-- Resource jQuery -->
</body>
</html>