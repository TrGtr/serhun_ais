<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5 Transitional//EN" "http://www.w3.org/TR/html5/loose.dtd">
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/reset.css">
<!-- CSS reset -->
<link rel="stylesheet" href="css/style.css">
<!-- Resource style -->
<script src="js/modernizr.js"></script>
<!-- Modernizr -->

<title>AIS</title>
</head>
<body>
	<header class="cd-main-header"> <a href="#0" class="cd-logo"><img
		src="img/cd-logo.svg" alt="Logo"></a>

	
	<!-- cd-search --> <a href="#0" class="cd-nav-trigger">Menu<span></span></a>

	<nav class="cd-nav">
	<ul class="cd-top-nav">
		<li class="has-children account"><a href="#0">
		<%
		HttpSession se = request.getSession();
		if (se.getAttribute("phone") == null) {
            response.sendRedirect("authorize.jsp");
        }
		out.println(se.getAttribute("role")+"("+se.getAttribute("phone")+")");
		if (!se.getAttribute("role").equals("manager") && !se.getAttribute("role").equals("courier")) {
			response.sendRedirect("items.jsp");
		}
		%> 
		</a>
			<ul>
				<li><a href="auth">Logout</a></li>
			</ul></li>
	</ul>
	</nav> </header>
	<!-- .cd-main-header -->

	<main class="cd-main-content"> <nav class="cd-side-nav">

	<ul>
		<li class="cd-label">Menu</li>
		<li class="has-children overview active"><a href="#0" id='overvw'>Overview</a>

			<ul>
			   <%if (se.getAttribute("role").equals("customer")) {
                out.println("<li><a href='items.jsp'>Items</a></li>");
                out.println("<li><a href='orders.jsp'>Orders</a></li>");
            }
            else {
                out.println("<li><a href='products.jsp'>Products</a></li>");
                out.println("<li><a href='items.jsp'>Items</a></li>");
                out.println("<li><a href='orders.jsp'>Orders</a></li>");
                out.println("<li><a href='couriers.jsp'>Couriers</a></li>");
                out.println("<li><a href='managers.jsp'>Managers</a></li>");
                out.println("<li><a href='customers.jsp'>Customers</a></li>");
            }
            %>
			</ul></li>
	</ul>

	<ul>
		<li class="cd-label">Actions</li>
		<li class="action-btn" id='edbtn'><a href="#0">Edit mode</a></li>
		<li><br></li>
		<li class="action-btn hidden" id='savebtn'><center>
				<input type="submit" form="myform" value="Save changes">
			</center></li>
	</ul>
	</nav>

	<div class="content-wrapper">
		<h1>Couriers</h1>
		<form id="myform" action="update" method="POST">
			<%
				ua.com.serhun.dao.CourierDao bdao = new ua.com.serhun.dao.CourierDaoImpl();
				try {
					out.println("<table id=\"courierTable\" class=\"input-list style-1 clearfix\">");
					out.println("<tr>");
					out.println("<th>courier ID</th>");
					out.println("<th>name</th>");
					out.println("<th>phone</th>");
					out.println("<td>row action</td>");
					out.println("</tr>");
					java.util.List<ua.com.serhun.domain.Courier> couriers = new java.util.LinkedList<ua.com.serhun.domain.Courier>();
					couriers.addAll(bdao.getAllCouriers());
					int i = 0;
					for (ua.com.serhun.domain.Courier courier : couriers) {
						++i;

						out.print("<tr>");
						out.println("<td>" + courier.getId() + "</td>");
						out.println("<input type='hidden' name='" + i
								+ "id' value='" + courier.getId() + "'>");
						out.println("<td class='cb' id='" + i + "name" + "'>"
								+ courier.getName() + "</td>");
						out.println("<td class='cb' id='" + i + "phone" + "'>"
								+ courier.getPhone() + "</td>");
						out.println("<td width='1%'><input type='submit' class='delbtn' id='"
								+ courier.getId()
								+ "' form='myform' value='delete'>"
								+ "</td>");

						out.print("</tr>");

					}
					out.println("</table>");
					//out.println("<p><small>" + couriers.size() + " entries found.</small></p>");
					out.println("<input type='hidden' name='count' value='"
							+ couriers.size() + "'>");
					out.println("<input type='hidden' name='from' value='couriers'>");
					out.println("<input type='hidden' id='action' name='action' value='upd'>");
					out.println("<input type='hidden' id='id' name='id' value=''>");
				} catch (ua.com.serhun.dao.DAOException e) {
					out.println("An Error occured when connecting to DataBase: "
							+ e.getLocalizedMessage());
				}
			%>
		</form>
	</div>
	<!-- .content-wrapper --> </main>
	<!-- .cd-main-content -->
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.menu-aim.js"></script>
	<script src="js/main.js"></script>
	<!-- Resource jQuery -->
</body>
</html>