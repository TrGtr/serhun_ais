<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700'
	rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="css/reset.css">
<!-- CSS reset -->
<link rel="stylesheet" href="css/style.css">
<!-- Resource style -->
<script src="js/modernizr.js"></script>
<!-- Modernizr -->

<title>AIS</title>
</head>
<body>
	<header class="cd-main-header"> <a href="#0" class="cd-logo"><img
		src="img/cd-logo.svg" alt="Logo"></a> <a href="#0"
		class="cd-nav-trigger">Menu<span></span></a> <nav class="cd-nav">
	<ul class="cd-top-nav">
		<li class="has-children account"><a href="#0"> <%
 	HttpSession se = request.getSession();
  	if (se.getAttribute("phone") == null) {
  		response.sendRedirect("authorize.jsp");
  	}
  	out.println(se.getAttribute("role") + "("
  			+ se.getAttribute("phone") + ")");
 %>
		</a>

			<ul>
				<li><a href="auth">Logout</a></li>
			</ul></li>
	</ul>
	</nav> </header>
	<!-- .cd-main-header -->

	<main class="cd-main-content"> <nav class="cd-side-nav">

	<ul>
		<li class="cd-label">Menu</li>
		<li class="has-children overview active"><a href="#0" id='overvw'>Overview</a>

			<ul>
				<%
					if (se.getAttribute("role").equals("customer")) {
								out.println("<li><a href='items.jsp'>Items</a></li>");
								out.println("<li><a href='orders.jsp'>Orders</a></li>");
							} else {
								out.println("<li><a href='products.jsp'>Products</a></li>");
								out.println("<li><a href='items.jsp'>Items</a></li>");
								out.println("<li><a href='orders.jsp'>Orders</a></li>");
								out.println("<li><a href='couriers.jsp'>Couriers</a></li>");
								out.println("<li><a href='managers.jsp'>Managers</a></li>");
								out.println("<li><a href='customers.jsp'>Customers</a></li>");
							}
				%>
			</ul></li>

	</ul>

	<ul>

		<li class="cd-label">Actions</li>
		<%
			if (!se.getAttribute("role").equals("customer")) {
				out.println("<li class='action-btn' id='edbtn'><a href='#0'>Edit mode</a></li>");
			} else {
				out.println("<li class='action-btn' id='orderbtn'><center><input type='submit' class='delbtn' form='myform' value='Make order'></center></li>");
			}
		%>

		<li><br></li>
		<li class="action-btn hidden" id='savebtn'><center>
				<input type="submit" form="myform" value="Save changes">
			</center></li>
	</ul>
	</nav>

	<div class="content-wrapper">
		<h1>Items</h1>

		<%
			if (!se.getAttribute("role").equals("customer")) {
				out.println("<form id='myform' action='update' method='POST'>");
			}
			ua.com.serhun.dao.ItemDao bdao = new ua.com.serhun.dao.ItemDaoImpl();
			try {
				out.println("<table id=\"itemsTable\" class=\"input-list style-1 clearfix\">");
				out.println("<tr>");
				out.println("<th>product ID</th>");
				out.println("<th>item ID</th>");
				out.println("<th>name</th>");
				out.println("<th>description</th>");
				out.println("<th>color</th>");
				out.println("<th>price</th>");
				if (!se.getAttribute("role").equals("customer")) {
					out.println("<th>order ID(0 if available)</th>");
					out.println("<td>row action</td>");
				} else {
					out.println("<td>add to order</td>");
				}

				out.println("</tr>");
				java.util.List<ua.com.serhun.domain.Item> items = new java.util.LinkedList<ua.com.serhun.domain.Item>();
				items.addAll(bdao.getAllItems());
				int i = 0;
				for (ua.com.serhun.domain.Item item : bdao.getAllItems()) {
					++i;
					out.print("<tr>");
					if (se.getAttribute("role").equals("customer")) {
						if (item.getIdorder() == 0) {
							out.println("<td>" + item.getIdproduct() + "</td>");
							out.println("<input type='hidden' name='" + i
									+ "idproduct' value='"
									+ item.getIdproduct() + "'>");
							out.println("<input type='hidden' name='" + i
									+ "iditem' value='" + item.getIditem()
									+ "'>");
							out.println("<td>" + item.getIditem() + "</td>");
							out.println("<td id='" + i + "name" + "'>"
									+ item.getName() + "</td>");
							out.println("<td id='" + i + "description" + "'>"
									+ item.getDescription() + "</td>");
							out.println("<td class='cb' id='" + i + "color"
									+ "'>" + item.getColor() + "</td>");
							out.println("<td id='" + i + "price" + "'>"
									+ item.getPrice() + "</td>");
							out.println("<td width='1%'><button class='addbtn' id='"
									+ item.getIditem()
									+ "' >add</button>"
									+ "</td>");
						}
					} else {
						out.println("<td>" + item.getIdproduct() + "</td>");
						out.println("<input type='hidden' name='" + i
								+ "idproduct' value='" + item.getIdproduct()
								+ "'>");
						out.println("<input type='hidden' name='" + i
								+ "iditem' value='" + item.getIditem() + "'>");
						out.println("<td>" + item.getIditem() + "</td>");
						out.println("<td id='" + i + "name" + "'>"
								+ item.getName() + "</td>");
						out.println("<td id='" + i + "description" + "'>"
								+ item.getDescription() + "</td>");
						out.println("<td class='cb' id='" + i + "color" + "'>"
								+ item.getColor() + "</td>");
						out.println("<td id='" + i + "price" + "'>"
								+ item.getPrice() + "</td>");
						out.println("<td class='cb' id='" + i + "idorder"
								+ "'>" + item.getIdorder() + "</td>");
						out.println("<td width='1%'><input type='submit' class='delbtn' id='"
								+ item.getIditem()
								+ "' value='delete' form='myform'>" + "</td>");
					}

					out.print("</tr>");

				}
				out.println("</table>");
				out.println("<input type='hidden' name='count' value='"
						+ items.size() + "'>");
				out.println("<input type='hidden' name='from' value='items'>");
				out.println("<input type='hidden' id='action' name='action' value='upd'>");
				out.println("<input type='hidden' id='id' name='id' value=''>");
				if (se.getAttribute("role").equals("customer")) {
			                out.println("<form id='myform' action='makeOrder' method='POST'>");
			                out.println("<input type='hidden' id='orderItems' name='orderItems' value=''>");
			                out.println("<br><center>Payment method: <select name='pmethod'>");
			                out.println("<option>cash</option><option>credit card</option></select>");
			                out.println("</form>");
			            }

			} catch (ua.com.serhun.dao.DAOException e) {
				out.println("An Error occured when connecting to DataBase: "
						+ e.getLocalizedMessage());
			}
			if (!se.getAttribute("role").equals("customer")) {
				out.println("</form>");
			}
		%>

	</div>
	<!-- .content-wrapper --> </main>
	<!-- .cd-main-content -->
	<script src="js/jquery-2.1.4.js"></script>
	<script src="js/jquery.menu-aim.js"></script>
	<script src="js/main.js"></script>
	<!-- Resource jQuery -->
</body>
</html>